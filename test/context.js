var rewire = require('rewire'),
    path = require('path');

module.exports = {
    require: function(module_path) {
        return rewire(path.resolve(__dirname, "../src", module_path));
    }
};
