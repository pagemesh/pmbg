var P = require('bluebird'),
    _ = require('lodash');

function handle($, $section, attributes) {
    var $label = $section.next('span');
    var classes = (attributes.classes || "").split(';').join(" ");
    $label.addClass('label ' +classes);
    $section.remove();
}

module.exports = {
    handle: handle
};
