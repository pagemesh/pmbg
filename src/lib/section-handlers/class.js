var P = require('bluebird'),
    _ = require('lodash');

function handle($, $section, attributes) {
    var $parent = $section.parents(attributes['apply-to']);

    if(attributes && attributes.classes) {
        $parent.addClass(function(){
            return attributes.classes.split(';').join(" ");
        });
    }
    $section.remove();
}

module.exports = {
    handle: handle
};
