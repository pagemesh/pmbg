var P = require('bluebird'),
    _ = require('lodash');

function handle($, $section, attributes) {
    var $para = $section.parent();
    var $endPara = $para.nextAll().find("span:contains('%[END-BLOCK')").parent();

    if($endPara.length === 0) {
        throw new Error("Unable to find %[END-BLOCK]");
    }

    $endPara.attr('data-end-block', true);
    var $content = $para.nextUntil("p[data-end-block=true]");
    var content = _.template("<div class='chapter-block' style=\"<%_.forEach(_.keys(attributes),function(key, idx){%><%if(idx>0){%>;<%}%><%=key%>:<%= attributes[key]%><%})%>\">")({attributes:attributes});
    $content.each(function(idx, c) {
        content += $(c).toString();
    });

    $para.replaceWith(content);
    $content.remove();
}

module.exports = {
    handle: handle
};
