var P = require('bluebird'),
    _ = require('lodash');

function handle($, $section, attributes) {
    var $para = $section.parent('p');
    var $page = $section.parents('.page');
    var $img = $para.find('img').get(0);
    var src = $img.attribs.src;
    var newContent = _.template("<div class='banner'><img src='<%- src %>' style=\"<%_.forEach(_.keys(attributes),function(key, idx){%><%if(idx>0){%>;<%}%><%=key%>:<%= attributes[key]%><%})%>\"></div>")({src: src, attributes: attributes});
    console.log("Replacing content with", newContent);
    $page.prepend(newContent);
    $para.remove();
}

module.exports = {
    handle: handle
};
