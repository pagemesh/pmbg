var P = require('bluebird');
var fs = require('fs');
var path = require('path');
var Hbs = require('handlebars');
var _ = require('lodash');
var dir = require('node-dir');
var Yaml = require('js-yaml');
var marked = require('marked');
var rest = require('restler');
var request = require('request');
var cssmin = require('cssmin');
var cheerio = require('cheerio');

var google = require('googleapis');

function BG(opts) {
    opts = opts || {};
    this.ctx = opts.ctx;
    this.ctx.auth = opts.auth;

    if(!opts.ctx) {
        throw new Error("Generation context is required");
    }

    if(this.ctx.auth) {
        this.ctx.drive = google.drive({ version: 'v2', auth: this.ctx.auth });
    }
}

var renderer = new marked.Renderer();


marked.setOptions({
    renderer: renderer,
    gfm: true,
    tables: true,
    breaks: false,
    pedantic: false,
    sanitize: false,
    smartLists: true,
    smartypants: true
});

function slugify() {
    var text = Array.prototype.slice.call(arguments).join("");
    return text.toLowerCase().replace(/ /g, '_').replace(/[^\w]+/g, '');
}

function loadTemplate(tmplPath) {
    return new P(function(resolve){
        resolve(P.promisify(fs.readFile, fs)(tmplPath,"utf8").then(function(tmplContent) {
            return Hbs.compile(tmplContent);
        }));
    });
}

function loadContent(ctx, pattern, asArray) {
    return new P(function(resolve, reject) {
        var result = {};

        dir.readFiles(ctx.src_path, { match: pattern }, function(err, content, filename, next) {
            if(err) {
                return reject(err);
            }

            var entryName = filename.match(pattern)[1];
            if(entryName.indexOf('/') !== -1) {
                var fragments = entryName.split('/');
                entryName = fragments[fragments.length-1];
            }
            console.log("Adding content entry %s to book content structure", entryName);
            result[entryName] = marked(content);

            next();
        }, function() {
            if(asArray) {
                var results = [];
                var headingPattern = new RegExp("<h([2-4]?)[^>]*>\\s*([^>]*?)<\\/h[2-4]>", "g");

                _.each(_.keys(result), function(entryKey, idx) {
                    var entry = {
                        id: slugify(entryKey),
                        title: entryKey,
                        label: entryKey,
                        no: idx+1,
                        content: result[entryKey],
                        toc_entries: []
                    };

                    entry.toc_entries.push({
                        id:entry.id,
                        label: entry.label,
                        title: entry.title,
                        level: 1
                    });

                    // Append a id to all heading elements (to build the TOC)
                    console.log("Producing heading ids and TOC entries for ", entry.label);
                    entry.content = entry.content.replace(headingPattern, function(tag, level, content) {
                        var id = _.uniqueId(slugify(entry.id, content));
                        var output = '<h'+level+' id="'+id+'">'+content+'</h'+level+'>';
                        entry.toc_entries.push({
                            id: id,
                            level: level,
                            label: content
                        });
                        return output;
                    });

                    results.push(entry);
                });

                resolve(results);
            }
            else {
                resolve(result);
            }
        });
    });
}

function extractTocEntries(content, level) {
    return new P(function(resolve) {
        var toc = [];

        _.each(content.chapters, function(chapter) {
            toc = toc.concat(chapter.toc_entries || []);
        });

        resolve(_.filter(toc, function(t){ return t.level <= level }));
    });
}

// Send the html file to DocRaptor
function generatePDF(ctx, bookinfo, book) {

    return new P(function(resolve, reject) {
        var req = rest.postJson("https://docraptor.com/docs", {
            user_credentials: ctx.docraptor_key,
            doc: {
                document_content: book,
                name:             bookinfo.filename,
                document_type:    "pdf",
                test:             ctx.test
            }
        });

        req.on('success', function(data, response) {
            resolve(response.raw);
        });

        req.on('fail', function(data) {
            reject(data);
        });

        req.on('error', reject);
    });
}

function embedStylesheets(ctx, content) {
    return new P(function(resolve) {
        var pattern = new RegExp('<link(.*?)>', 'g');
        var stylesheets = [];

        // Retrieve the list of stylesheets to embed
        var links = content.match(pattern);
        if(links) {
            links = _.map(links, function(ss) {
                var url = ss.match(/href="(.*?)"/)[1];
                stylesheets.push({
                    link: ss,
                    url: url
                });
                return url;
            });

            P.each(stylesheets, function(sheet) {
                return new P(function(resolve, reject) {
                    console.log("Requesting document: ", sheet.url);
                    if(sheet.url.indexOf('http') >= 0) {
                        request({
                            method: 'get',
                            url: sheet.url
                        }, function(err, response, body) {
                            sheet.content = Hbs.compile(body)(ctx.bookinfo.theme);
                            resolve(sheet);
                        });
                    }
                    else if(sheet.url.indexOf('source:') >=0 ) {
                        P.promisify(fs.readFile, fs)(path.resolve(ctx.src_path, sheet.url.substring(7)), "utf8").then(function(content) {
                            sheet.content = Hbs.compile(content)(ctx.bookinfo.theme);
                            resolve(sheet);
                        });
                    }
                    else {
                        P.promisify(fs.readFile, fs)(path.resolve(ctx.theme_path, sheet.url), "utf8").then(function(content) {
                            sheet.content = Hbs.compile(content)(ctx.bookinfo.theme);
                            resolve(sheet);
                        });
                    }
                });
            }).then(function() {
                _.each(stylesheets, function(sheet) {
                    content = content.replace(sheet.link, "<style>"+cssmin(sheet.content)+"</style>");
                });
                resolve(content);
            });
        }

    });
}

function embedImages(ctx, content) {
    return new P(function(resolve) {

        resolve(content);
    });
}

function prepareTemplateEngine(ctx) {
    return new P(function(resolve, reject) {
        dir.readFiles(ctx.partial_path, function(err, content, filename, next) {
            if(err) {
                return reject(err);
            }

            var match = filename.match(/([^/]*?)\.hbs$/);
            if (match) {
                var partialKey = match[1];
                Hbs.registerPartial(partialKey, content);
            }
            next();
        }, function() {
            resolve();
        });
    });
}

function processSpecialSections($) {

    return new P(function(resolve, reject) {
        console.log("Processing special sections");

        var $sections = $('body').find("span:contains('![')"), error = false;
        while($sections.length > 0 && !error) {
            var $section = $($sections.get(0));

            // Find the section type
            // ![BANNER {height: 2.5in }]
            var elements = $section.html().match(/!\[(.*?)(\s(.*?))?\]/);
            if(elements) {
                var sectionType = elements[1].toLowerCase(), pattern = new RegExp("([^{\\s]*?)\\s*:\\s*(.*?)[,\\s}]+", "g");
                var attributes = {};

                while(match = pattern.exec(elements[2])) {
                    attributes[match[1]] = match[2];
                }

                try {
                    var handler = require(path.resolve(__dirname, './section-handlers/'+sectionType));
                    handler.handle($, $section, attributes);
                }
                catch(err) {
                    console.log("Unsupported handler specified".red, elements[1].red,". No further special sections will be processed".red, err);
                    error = true;
                }
            }
            else {
                console.log("Cannot parse:", $section.html());
                error = true;
            }
            $sections = $('body').find("span:contains('![')");
        }

        // Remove all ending block markers
        $("span:contains('%[END-BLOCK')").remove();

        resolve($.html());
    });

}

function convertTo(ctx, file, mimeType) {

    return new P(function(resolve, reject) {
        var exportLink = file.exportLinks[mimeType];
        if(exportLink) {
            request({method:'get', url:exportLink, qs: { access_token: ctx.auth.credentials.access_token }}, function(err, resp, body) {
                if(err) {
                    return reject(err);
                }

                var $ = cheerio.load(body, {normalizeWhitespace: true, decodeEntities: false, recognizeSelfClosing: true});

                resolve($('body').html());

            });
        }
        else {
            resolve("");
        }
    });
}

function createEntry(ctx, file, idx) {
    console.log("Creating content entry %s (%d)", file.title, idx);
    var entryId = slugify(file.title);

    var toc_entries = [{
        id: entryId,
        label: file.title.match(/([A-Z]{2}?)-\s*(.*?)$/)[2],
        level: 1
    }];

    return P.props({
        id: entryId,
        title: file.title,
        label: file.title,
        content: convertTo(ctx, file, 'text/html').then(function(html) {
            var headingPattern = new RegExp("<h([2-4]?)[^>]*>(?:.*?)?(<span>)?\\s*([^>]*?)(<\\/span>)?<\\/h[2-4]>", "g");

            // Append a id to all heading elements (to build the TOC)
            console.log("Producing heading ids and TOC entries for ", file.title);
            return html.replace(headingPattern, function(tag, level, span, content) {
                var id = _.uniqueId(slugify(entryId, content));
                var output = '<h'+level+' id="'+id+'">'+content+'</h'+level+'>';

                toc_entries.push({
                    id: id,
                    level: level,
                    label: content
                });

                return output;
            });

        })
    }).then(function(entry) {
        entry.toc_entries = toc_entries;

        if(idx) {
            entry.no = idx+1;
        }

        return entry;
    });
}

function createBookStructure(ctx) {

    if(ctx.google_drive) {
        var q = "mimeType = 'application/vnd.google-apps.document' and '"+ctx.google_drive_folder+"' in parents";

        return P.promisify(ctx.drive.files.list,ctx.drive.files)({q:q}).spread(function(files) {

            var book = {chapters: []};

            return P.each(files.items, function(file) {
                if(file.title.match(/credits/)) {
                    book.credits = createEntry(ctx, file);
                }
                else if(file.title.match(/preface/)) {
                    book.preface = createEntry(ctx, file);
                }
                else {
                    book.chapters.push(createEntry(ctx, file, book.chapters.length));
                }
            }).then(function() {
                return P.props(book).then(function(b) {
                    return P.all(b.chapters).then(function(chapters) {

                        // Sort all chapters
                        b.chapters = _.sortBy(chapters, function(c) {
                            var elms = c.title.match(/([A-Z]{2}?)-\s*(.*?)$/);
                            if(elms) {
                                c.title = c.label = elms[2];
                                return elms[1];
                            }

                            return c.no;
                        });

                        // Post Process chapters
                        b.chapters = _.map(b.chapters, function(c, idx) {
                            c.no = idx +1;
                            return c;
                        });

                        return b;
                    });
                });
            });

        });
    }
    else {
        return P.props({
            credits: loadContent(ctx, /credits-(.*?)\.md.txt$/),
            preface: loadContent(ctx, /preface-(.*?)\.md.txt$/),
            chapters: loadContent(ctx, /[A-Z]{2}-\s*(.*?)\.md\.txt$/, true)
        });
    }
}

BG.prototype.generate = function() {
    var _this = this;

    // Load the book metadata
    var bookinfo = Yaml.safeLoad(fs.readFileSync(path.resolve(_this.ctx.src_path, "book.yaml"), "utf8"));

    if(bookinfo.theme && bookinfo.theme.path) {
        this.ctx.theme_path = path.resolve(bookinfo.theme.path);
    }
    else {
        // Use our internal theme
        this.ctx.theme_path = path.resolve(__dirname, "../../theme");
    }

    this.ctx.bookinfo = bookinfo;
    this.ctx.partial_path = path.join(this.ctx.theme_path, "partials");

    if(bookinfo.filename) {
        this.ctx.out_path = "./"+bookinfo.filename;
    }

    // Register all partials in our Hbs instance
    return prepareTemplateEngine(_this.ctx).then(function() {
        return loadTemplate(path.join(_this.ctx.theme_path, "layout.hbs"))
    }).then(function(layout) {

        if(bookinfo.google && bookinfo.google.src_folder) {
            _this.ctx.google_drive_folder = bookinfo.google.src_folder;
        }

        // Create the book source structure
        return createBookStructure(_this.ctx).then(function(content) {
            console.log("Loaded %d chapters from %s", content.chapters.length, _this.ctx.src_path);

            // Extract all TOC entries
            return extractTocEntries(content, _this.ctx.toc_level).then(function(toc_entries) {
                content.toc_entries = toc_entries;

                // Render the book template
                var book = layout(_.merge(bookinfo, content));

                // Embed all stylesheets
                return embedStylesheets(_this.ctx, book).then(function(bookContent) {
                    // Embed all images (base64)
                    return embedImages(_this.ctx, bookContent);
                }).then(function(bookContent) {

                    console.log("Analyzing content for special sections");
                    var $ = cheerio.load(bookContent, {normalizeWhitespace: true, decodeEntities: false, recognizeSelfClosing: true});

                    return processSpecialSections($).then(function(content) {

                        if(_this.ctx.debug) {
                            fs.writeFileSync('./debug-book.html', content, 'utf8');
                        }

                        // Generate the print-ready PDF
                        if(!_this.ctx.simulate) {
                            return generatePDF(_this.ctx, bookinfo, content).then(function(binpdf) {

                                // Streams the PDF to the output file
                                return P.promisify(fs.writeFile, fs)(_this.ctx.out_path, binpdf, "binary");
                            });
                        }
                    });

                });

            });

        });

    });

};

module.exports = BG;
