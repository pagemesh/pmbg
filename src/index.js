#!/usr/bin/env node

/**
 * Copyright (c) 2015 Pagemesh Inc.

 This file is part of PMBG.

 PMBG is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
var args = require('minimist')(process.argv.slice(2), {});
var readline = require('readline');
var google = require('googleapis');
var OAuth2 = google.auth.OAuth2;
var BookGenerator = require('./lib/bookgen');
var path = require('path');
var fs = require('fs');
var pkg = require('../package.json');

require('colors');

var CLIENT_ID = process.env.GOOGLE_CLIENT_ID || "745253249637-n7j1inv63al8v8f64movve1ug3npoj45.apps.googleusercontent.com",
    CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET || args['google-secret'],
    REDIRECT_URL = process.env.GOOGLE_REDIRECT_URL || "urn:ietf:wg:oauth:2.0:oob",
    SCOPE = 'https://www.googleapis.com/auth/drive.readonly';

var ctx = {
    src_path: path.resolve(args.source || "."),
    out_path: path.resolve(args.output || "."),
    toc_level: args.toc ? parseInt(args.toc):3,
    test: typeof args.pub === 'undefined',
    docraptor_key: process.env.DOCRAPTOR_KEY || args['docraptor-key'],
    debug: args.debug,
    simulate: args.sim
};

if(!args.noBanner) {
    console.log("Pamemesh Book Generator ".bold.green);
    console.log("Version".bold, pkg.version);
    console.log("Licensed under the GPLv3. Please read the license before using this software".bold);
    console.log("");
}

function generateBook(ctx, auth) {
    var bg = new BookGenerator({
        auth: auth,
        ctx: ctx
    });

    return bg.generate().then(function() {
        console.log("Book %s was successfully generated".green, ctx.out_path.green);
        process.exit(0);
    }).catch(function(err) {
        console.log("ERROR: ".red, err.toString().red);
    });

}

if(args.google) {
    console.log("Google Drive mode enabled".yellow);

    // indicate that we want to retrieve all files the authorized google drive
    ctx.google_drive = true;
    ctx.google_drive_folder = args.folder;
    ctx.google_drive_pattern = args.include || /\.gdoc$/;

    var rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    var auth = new OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URL);
    var url = auth.generateAuthUrl({ scope: SCOPE, access_type: 'offline' });

    var getAccessToken = function(code) {
        rl.close();

        auth.getToken(code, function(err, tokens) {
            if (err) {
                console.log('Error while trying to retrieve access token', err);
                return;
            }
            auth.credentials = tokens;

            fs.writeFileSync(".pmbg_token", JSON.stringify(auth.credentials));

            generateBook(ctx, auth);
        });
    };

    // Check if we have a save token and that it is still valid
    if(fs.existsSync(".pmbg_token")) {
        try {
            var credentials = JSON.parse(fs.readFileSync(".pmbg_token", "utf8"));
            if(credentials) {

                // Do we have a valid token?
                if(credentials.expiry_date <= Date.now() ) {
                    auth.credentials = credentials;

                    auth.refreshAccessToken(function(err, tokens) {
                        if(err) {
                            console.log("ERROR".red, err.toString().red);
                            fs.unlinkSync(".pmbg_token");
                            console.log('Visit the url: '.yellow, url);
                            rl.question('Enter the code here:', getAccessToken);
                        }
                        else {
                            auth.credentials = tokens;
                            fs.writeFileSync(".pmbg_token", JSON.stringify(auth.credentials));
                            generateBook(ctx, auth);
                        }

                    });

                }
                else {
                    // Go directly to book generation
                    auth.credentials = credentials;

                    generateBook(ctx, auth);
                }
            }
        }
        catch(err) {
            console.log('Visit the url: '.blue, url.blue);
            rl.question('Enter the code here:', getAccessToken);
        }
    }
    else {
        console.log('Visit the url: ', url);
        rl.question('Enter the code here:', getAccessToken);
    }
}
else {
    generateBook(ctx);
}

