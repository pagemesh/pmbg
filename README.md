# Pagemesh Book Generator

## Getting Started

Install Pagemesh Book Generator through NPM

`npm install -g pagemesh-book-generator`

After this command, you should have the pmbg command in your PATH.
 
### Creating a print-reader PDF

You need to create an environment variable with your DocRaptor API KEY: 

`export DOCRAPTOR_KEY=321321321321321`
`pmbg --source <book-source-folder> --output <pdf-path>`

On Unix (and OSX) you can integrate both by using this command line: 

`DOCRAPTOR_KEY=121212211 pmbg --source <book-source-folder> --output <pdf-path>`

## Create a print-ready PDF using Google Drive

One interesting feature to ease the collaboration is to use Google Drive to store your book content. You work in Google Docs with your collaborators
and just point the generator to the right folder to produce your final print-ready PDF. A few extra authentication steps are required. 

### Environment Variables

- **GOOGLE_CLIENT_ID** : Just generate a google client ID to access your drive. Use Google Developer Console. 
- **GOOGLE_CLIENT_SECRET** : Secret is found in the console too
- **GOOGLE_REDIRECT_URL** : Redirect URL as provided by the google console.

### Running the generator

`pmbg --source ./my-wonderful-book --output ./my-wonderful-book.pdf --google`

At the start of the process, you'll be asked to navigate to a given URL. Just copy & paste this URL in your favorite browser and follow the steps. You'll complete by copying 
the provided in the waiting prompt and the book generation will continue. 

### Notes on Google Docs

For now, you should create one document per chapter. Each document should be named : AA-Title . The first two letters are used to sort chapters when building the book. They will
be removed from any title. You can create a file named AA-credits and BB-preface that will be processed using the credits and preface template instead of the normal chapter layout.
 
In order to further improve the layout, we are forced to add special tags to the Google docs. The following tags are supported by this version: 

- ![BANNER]: Take the next image in the documentation and bring it at the top of the chapter page. 
- ![BLOCK {attributes}]: Define a block. All listed attributes will be applied as style on the block. This is a good way to control column layouts, margin and other flow aspects.
- ![CLASS {classes,apply-to}]: Take a semi-colon separated list of class names and apply them to the first parent, optionnaly matching apply-to selector. This is helpful to inject
classes dynamically in a document. Used for list (UL) formatting for example. 
- ![LABEL {classes}]: Mark the next span with the label class. You can also add arbitrary classes to the span. 

All added classes may be defined in an extra CSS you maintain in your book local theme folder. 

## Book Source Structure

The folder provided with the --source parameter should have the following structure: 
 
```
book.yaml
assets/<images>
theme
    book.css
```

### Book.yaml

This yaml file is used throughout the generation process as the source of external template data. You can put about anything required by your theme here. 

The following fields are used in the default theme and generation process: 

| Field | Type | Notes |
| ----- |:----:|: ---- |
| title | string | Book title |
| author | string | Primary book author. Will be written on the cover page |
| filename | string | The default filename if not specified through the --output parameter |
| google | object | (optional) Used only if your book sources are in a google drive folder |
| google.src_folder | string | The id of the folder containing all your chapters. Get the id using the sharing tool in Google Drive |
| theme | object | Theme configuration |
| theme.extra_css | list | A list of CSS to add to the book file. Use source: for CSS located in your source folder |
| theme.page_size | string | The page size. See PrinceXML supported page sizes | 
| theme.cover_url | url | The full URL of the cover to use. Local url are not supported right now. Use Cloudinary if necessary | 
| theme.page_background_url | url | The url of a picture used as page background (for all pages) |
| theme.font.url | url | The path of a Google Font CSS to load to get access to the book font |
| theme.font.family | string | The family to use in the theme CSS file |
| theme.font.size | string | The font size for the normal text |
| theme.colors.text | string | The color of the book text. Use cmyk codes here. ex: cmyk(0, 0, 0, 1) |
| theme.colors.heading | string | The color of the book headings. Use cmyk codes here. ex: cmyk(0, 0, 0, 1) |
| credits | object | A hash of all credits information. See theme for supported fields |
| pubdate | date | Publishing date |
| copyright | string or array | Arbitrary copyright text add to the credits page. Arrays are joined |

